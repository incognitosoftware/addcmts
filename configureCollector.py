#!/usr/bin/python

import xml.etree.ElementTree as ET
import sys, os
import xmleditor.CommentBuilder as CommentBuilder
import xmleditor.ConfigurationEditor as configEditor
from argparse import ArgumentParser
import ConfigParser

parser = ArgumentParser()
parser.add_argument("--cmts-host", dest="newCMTSIP",help="CMTS Hostname or IP Address")
parser.add_argument("--cmts-port", dest="newCMTSPort",help="CMTS Port", default="4737")

args=parser.parse_args(args=None if sys.argv[1:] else ['--help'])

print args

if len(sys.argv) <> 5:
 parser.print_help()
 sys.exit(1)
 
config = ConfigParser.RawConfigParser()
config.read('configure-cmts.cfg')

collectorConfigFileName=config.get('IPDR-Collector','collectorPath')

newCMTSIP=args.newCMTSIP
newCMTSPort=args.newCMTSPort

def main(args=None):

    if(os.path.isfile(collectorConfigFileName)):
       print 'Collector config exists:' + collectorConfigFileName
    else :
       print 'Collector config does not exist in:' + collectorConfigFileName + '. Exiting..'
       sys.exit(1)
   
    parser=CommentBuilder.CommentedTreeBuilder()
    
    # Open original file
    et = ET.parse(collectorConfigFileName,parser)
    root=et.getroot()
    
    hosts = []
    addCMTS=True
    
    for host in root.iter('host'):
        hosts.append(host.text)
        if(newCMTSIP + ':' + newCMTSPort == host.text):
             print('CMTSIP:' + newCMTSIP + ' is already in the collector' )
             addCMTS=False
             break

    if addCMTS:
        configEditor.addNewCollector(newCMTSIP,newCMTSPort,et,collectorConfigFileName) ## call a method.
    else:
        print 'CMTS already added'

main()

