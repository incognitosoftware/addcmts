#!/usr/bin/python


import xml.etree.ElementTree as ET
import sys, os
import xmleditor.CommentBuilder as CommentBuilder
import xmleditor.ConfigurationEditor as configEditor
from argparse import ArgumentParser
import ConfigParser


config = ConfigParser.RawConfigParser()
config.read('configure-cmts.cfg')

defPort = config.get('IPDR-Proxy', 'defaultCMTSPort')
proxyConfigFileName=config.get('IPDR-Proxy','proxyPath')
remotehost=config.get('IPDR-Proxy','remoteHost')

parser = ArgumentParser()
parser.add_argument("--cmts-host", dest="newCMTSIP",help="CMTS HostName or IP Address")
parser.add_argument("--cmts-port", dest="newCMTSPort",help="CMTS Port", default="4737")
parser.add_argument("--collector-host", dest="newCollectorIP",help="Collector IP Address")
parser.add_argument("--local-addr-port", dest="newProxyPort",help="Proxy Port for local-addr")
parser.add_argument("--collector-priority", dest="newCollectorPriority",help="Priority for the new collector")

args=parser.parse_args(args=None if sys.argv[1:] else ['--help'])

if len(sys.argv) <> 11:
 parser.print_help()
 sys.exit(1)

newCMTSIP=args.newCMTSIP
newCMTSPort=args.newCMTSPort
newCollectorIP=args.newCollectorIP
newProxyPort=args.newProxyPort
newCollectorPriority=args.newCollectorPriority

def iterparent(tree):
    for parent in tree.getiterator():
        for child in parent:
            yield parent, child

def confProxy(args=None):
    try:
       if(os.path.isfile(proxyConfigFileName)):
          print 'Proxy config exists:' + proxyConfigFileName
    except IOError:
          print 'Proxy config file:' + proxyConfigFileName + ' does not exist. EXITING...'
    
    parser=CommentBuilder.CommentedTreeBuilder()
    
    # Open original file
    et = ET.parse(proxyConfigFileName,parser)
    root=et.getroot()
    
    remoteAddrs = []
    addCMTS=True
    
    for remoteAddr in root.iter('remote-addr'):
        remoteAddrs.append(remoteAddr.text)
       
    for index, item in enumerate(remoteAddrs):
         print index, item
         if(newCMTSIP in item):
             print('CMTSIP:' + newCMTSIP + ' is already in the proxy' )
             addCMTS=False
             collectorToModify=remoteAddrs[index]
             break
    
    ## Open question: which collectors (set) should we add the new collector.
    ### Add it to the first collector set
    if addCMTS:
        configEditor.addNewProxy(newCMTSIP,newCMTSPort,newCollectorIP,newProxyPort, newCollectorPriority,et,proxyConfigFileName) ## call a method.
    else:
        print 'CMTS already configured, Checking if collector needs to be added in:' + collectorToModify
       
        for parent, child in iterparent(et):
            if (child.text == collectorToModify):
                print parent, child
                break
    
        proxy=parent
       
        collectorColl = proxy.findall(".sessions/collectors/collector")
    
        addCollector=True
       
        for index, item in enumerate(collectorColl):
           print index, item[0].text
           if(newCollectorIP == item[0].text):
              print 'Collector:' , newCollectorIP , ' already in config'
              addCollector=False
              break
    
        if(addCollector):
            collectors=proxy.findall(".sessions/collectors")
            for index, item in enumerate(collectors):
                print index, item[0].text
                collectorsToAdd=item
    
        if(addCollector):
            configEditor.addNewCollectorInProxy(collectorsToAdd,newCollectorIP,newCollectorPriority, et,proxyConfigFileName)
    
    ## Verify the file was updated correctly 
    os.system('xmllint "%s" > testProxyXML.out' % (proxyConfigFileName) )
    os.system('grep "%s" testProxyXML.out' %(newCollectorIP) )

    ## Copy the file to a remote proxy server
    #os.system('rsync -avzhe ssh config.xml root@mapdev.qa.incognito.com:~/ 1>/dev/null')
    #os.system('scp "%s" "%s:%s"' % (proxyConfigFileName, remotehost, proxyConfigFileName) )

def reload (args=None):
  ## Now reloading incognito-ipdr-proxy service
  print('Now reloading incognito-ipdr-proxy service')
  os.system('systemctl reload incognito-ipdr-proxy')

if __name__ == '__main__':
  confProxy()

if __name__ == 'reload':
  reload()
