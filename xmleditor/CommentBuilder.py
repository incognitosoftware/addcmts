import sys, os
import xml.etree.ElementTree as ET

### Parser to retain comments
class CommentedTreeBuilder(ET.XMLTreeBuilder):
    def __init__(this, *args, **kwargs):
        super(CommentedTreeBuilder, this).__init__(*args, **kwargs)
        this._parser.CommentHandler = this.comment

    def comment(this, data):
        this._target.start(ET.Comment, {})
        this._target.data(data)
        this._target.end(ET.Comment)
