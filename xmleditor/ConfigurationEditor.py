#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 15:31:11 2018

@author: vishal.thenge
"""
import xml.etree.ElementTree as ET
import os
from datetime import datetime
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('configure-cmts.cfg')

comment = ET.Comment(' === Updated by IncognitoAddCMTS script ==== ')

def indent(elem, level=0):
     i = "\n" + level*"  "
     if len(elem):
       if not elem.text or not elem.text.strip():
         elem.text = i + "  "
       if not elem.tail or not elem.tail.strip():
          elem.tail = i
          for elem in elem:
            indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
              elem.tail = i
            else:
             if level and (not elem.tail or not elem.tail.strip()):
               elem.tail = i
              
              
def addNewProxy(newCMTSIP,newCMTSPort,newCollectorIP,newCollectorPort,newCollectorPriority, et, configFileName):
    
        print('Proxy config will be updated for:' + newCMTSIP + ':' + newCMTSPort + ' with priority:' + newCollectorPriority)

        root=et.getroot()
        
        proxy = ET.SubElement(root, 'proxy')

        proxy.append(comment)
        remoteAddr = ET.SubElement(proxy,'remote-addr')
        remoteAddr.text=newCMTSIP+":"+newCMTSPort
        
        localAddr = ET.SubElement(proxy,'local-addr')
        localAddr.text='*:'+ newCollectorPort
        
        ipdrspSingleTcp = ET.SubElement(proxy,'ipdrsp-single-tcp')
        ipdrspSingleTcp.text='false'
       
        
        sessions = ET.SubElement(proxy,'sessions')
        if(config.has_option('IPDR-Proxy', 'samis')): 
           names= ET.SubElement(sessions,'name')
           names.text=config.get('IPDR-Proxy', 'samis')

        if(config.has_option('IPDR-Proxy', 'dsutil')):
           names= ET.SubElement(sessions,'name')
           names.text=config.get('IPDR-Proxy', 'dsutil')

        if(config.has_option('IPDR-Proxy', 'usutil')):
           names= ET.SubElement(sessions,'name')
           names.text=config.get('IPDR-Proxy', 'usutil')

        if(config.has_option('IPDR-Proxy', 'topology')):
           names= ET.SubElement(sessions,'name')
           names.text=config.get('IPDR-Proxy', 'topology')

        if(config.has_option('IPDR-Proxy', 'collectorName')):
           collectors= ET.SubElement(sessions,'collectors')
           collectorName =  ET.SubElement(collectors,'name')
           collectorName.text=config.get('IPDR-Proxy', 'collectorName')

        collector =  ET.SubElement(collectors,'collector')
        addr= ET.SubElement(collector,'addr')
        addr.text=newCollectorIP
       
        collectorPriority= ET.SubElement(collector,'priority')
        collectorPriority.text=newCollectorPriority
  
        root =et.getroot()
        indent(root)

        modifiedTime = os.path.getmtime(configFileName)
        timeStamp =  datetime.fromtimestamp(modifiedTime).strftime("%b-%d-%y-%H:%M:%S")
        os.rename(configFileName,configFileName+"_"+timeStamp)

        #et.write(configFileName, pretty_print=True)
        et.write(configFileName)
        #print minidom.parseString(ET.tostring(et)).toprettyxml()
               
def addNewCollector(newCMTSIP,newCMTSPort, et ,configFileName):
        print 'Collector config will be updated for :' + newCMTSIP + ':' + newCMTSPort
        root=et.getroot()
        collect = ET.SubElement(root, 'collect')
        collect.append(comment)
        cmts = ET.SubElement(collect,'cmts')
        host = ET.SubElement(cmts,'host')
        host.text=newCMTSIP+':'+newCMTSPort


        if(config.has_option('IPDR-Collector', 'samisSchemaName')):
            schema=ET.SubElement(collect,'schema')
            name=ET.SubElement(schema,'name')
            name.text=config.get('IPDR-Collector', 'samisSchemaName')
            if(config.has_option('IPDR-Collector', 'samisSchemaSession')):
               session=ET.SubElement(schema,'session')
               session.text=config.get('IPDR-Collector', 'samisSchemaSession')
            if(config.has_option('IPDR-Collector', 'samisExportDirectory')):
               exportDir=ET.SubElement(schema,'exportdir')
               exportDir.text=config.get('IPDR-Collector', 'samisExportDirectory')

        if(config.has_option('IPDR-Collector', 'topologySchemaName')):
            schema=ET.SubElement(collect,'schema')
            name=ET.SubElement(schema,'name')
            name.text=config.get('IPDR-Collector', 'topologySchemaName')
            if(config.has_option('IPDR-Collector', 'topologySchemaSession')):
               session=ET.SubElement(schema,'session')
               session.text=config.get('IPDR-Collector', 'topologySchemaSession')
            if(config.has_option('IPDR-Collector', 'topologyExportDirectory')):
               exportDir=ET.SubElement(schema,'exportdir')
               exportDir.text=config.get('IPDR-Collector', 'topologyExportDirectory')

        if(config.has_option('IPDR-Collector', 'usutilSchemaName')):
            schema=ET.SubElement(collect,'schema')
            name=ET.SubElement(schema,'name')
            name.text=config.get('IPDR-Collector', 'usutilSchemaName')
            if(config.has_option('IPDR-Collector', 'usutilSchemaSession')):
               session=ET.SubElement(schema,'session')
               session.text=config.get('IPDR-Collector', 'usutilSchemaSession')
            if(config.has_option('IPDR-Collector', 'usutilExportDirectory')):
               exportDir=ET.SubElement(schema,'exportdir')
               exportDir.text=config.get('IPDR-Collector', 'usutilExportDirectory')

        if(config.has_option('IPDR-Collector', 'dsutilSchemaName')):
            schema=ET.SubElement(collect,'schema')
            name=ET.SubElement(schema,'name')
            name.text=config.get('IPDR-Collector', 'dsutilSchemaName')
            if(config.has_option('IPDR-Collector', 'dsutilSchemaSession')):
               session=ET.SubElement(schema,'session')
               session.text=config.get('IPDR-Collector', 'dsutilSchemaSession')
            if(config.has_option('IPDR-Collector', 'dsutilExportDirectory')):
               exportDir=ET.SubElement(schema,'exportdir')
               exportDir.text=config.get('IPDR-Collector', 'dsutilExportDirectory')
        
        root =et.getroot()
        indent(root)

        modifiedTime = os.path.getmtime(configFileName) 
        timeStamp =  datetime.fromtimestamp(modifiedTime).strftime("%b-%d-%y-%H:%M:%S")
        os.rename(configFileName,configFileName+"_"+timeStamp)

        et.write(configFileName)
        
def addNewCollectorInProxy(collectors,newCollectorIP, newCollectorPriority, et, configFileName):
       print 'addNewCollectorInProxy'
       collectorToAdd= ET.SubElement(collectors,'collector') ## Add to the first collector/ element.
       collectorAddr= ET.SubElement(collectorToAdd,'addr')
       collectorAddr.text=newCollectorIP

       collectorPriority= ET.SubElement(collectorToAdd,'priority')
       collectorPriority.text=newCollectorPriority

       root =et.getroot()
       indent(root)
       et.write(configFileName)
